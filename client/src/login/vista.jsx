import React, { useState } from "react";

import "../App.css";

const Vista = (props) => {
  const [nombre, setNombre] = useState("");
  const [registrado, setRegistrado] = useState(false);

  const registrar = (e) => {
    e.preventDefault();
    if (nombre !== "") {
      setRegistrado(true);
    }
  };

  return (
    <div className="App">
      {!registrado && (
        <form onSubmit={registrar} className="inicio" style={{margin: "auto",width: "50%"}}>

          <div class="mb-3" >          
          <label htmlFor="" class="form-label">Nombre de Usuario</label>
          <input value={nombre} class="form-control" onChange={(e) => setNombre(e.target.value)} />
          
          </div>

          <button class="btn btn-info">Entrar Al chat</button>
        </form>
      )}

      
    </div>
  );
}

export default Vista;
